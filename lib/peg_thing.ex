defmodule PegThing do
  def start do
    prompt_rows()
    |> prompt_empty_peg()
    |> loop()
  end

  defp loop(:continue), do: start()
  defp loop(:exit), do: nil

  defp loop(board) do
    board
    |> prompt_move()
    |> next_move()
    |> loop()
  end

  defp prompt_rows do
    "How many rows? [5]: "
    |> normal_input("5")
    |> String.to_integer()
    |> PegThing.Board.new_board()
  end

  defp prompt_empty_peg(board) do
    PegThing.IO.puts("Here's your board:")
    PegThing.IO.render_board(board)

    input =
      "Remove which peg? [e]: "
      |> normal_input("e")
      |> PegThing.IO.l2p()

    PegThing.Peg.remove(board, input)
  end

  defp prompt_move(board) do
    PegThing.IO.puts("Here's your board:")
    PegThing.IO.render_board(board)

    case move_message() do
      [from, to] ->
        PegThing.Peg.make_move(board, from, to)

      _error ->
        PegThing.IO.puts("!!! That was an invalid move :(")
        board
    end
  end

  defp move_message do
    "Move from where to where? Enter two letters: "
    |> PegThing.IO.get_input()
    |> Enum.take(2)
    |> Enum.map(&PegThing.IO.l2p/1)
  end

  defp next_move(board) do
    if PegThing.Peg.can_move?(board) do
      board
    else
      prompt_endgame(board)
      game_over()
    end
  end

  defp prompt_endgame(board) do
    remaining =
      board
      |> Map.drop([:rows])
      |> Enum.filter(&elem(&1, 1)[:pegged])
      |> Enum.count()

    PegThing.IO.puts("Game Over! You had #{remaining} pegs left:")
    PegThing.IO.render_board(board)
  end

  defp game_over do
    case normal_input("Play again? y/n [y]: ", "y") do
      "y" -> :continue
      _ -> :exit
    end
  end

  defp normal_input(message, default) do
    input =
      message
      |> PegThing.IO.get_input()

    List.first(input) || default
  end
end
