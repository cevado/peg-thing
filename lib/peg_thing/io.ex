defmodule PegThing.IO do
  @alpha_start 96
  @pos_chars 3

  alias PegThing.Board

  def render_board(board) do
    1..board[:rows]
    |> Enum.map(&render_row(board, &1))
    |> Enum.join("\n")
    |> IO.puts()
  end

  def puts(message) do
    IO.puts(message)
  end

  def get_input(message) do
    message
    |> IO.gets()
    |> String.trim()
    |> String.graphemes()
  end

  def l2p(letter) do
    letter
    |> String.to_charlist()
    |> List.first()
    |> Kernel.-(@alpha_start)
  end

  def p2l(pos) do
    [pos + @alpha_start] |> to_string()
  end

  defp render_row(board, row) do
    line =
      row
      |> row_positions()
      |> Enum.map(&render_position(board, &1))
      |> Enum.join(" ")

    row_padding(row, board[:rows]) <> line
  end

  defp row_padding(row, rows) do
    padding = floor((rows - row) * @pos_chars / 2)
    String.duplicate(" ", padding)
  end

  defp row_positions(row) do
    (1 + (Board.triangular_row(row - 1) || 0))..Board.triangular_row(row)
  end

  defp render_position(board, pos) do
    peg = if board[pos][:pegged], do: blue("0"), else: red("-")
    p2l(pos) <> peg
  end

  defp red(text), do: colorize(text, :red)
  defp blue(text), do: colorize(text, :blue)

  defp colorize(text, color) do
    apply(IO.ANSI, color, []) <> text <> IO.ANSI.reset()
  end
end
