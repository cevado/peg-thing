defmodule PegThing.Board do
  def new_board(rows) do
    board = %{rows: rows}
    max_pos = triangular_row(rows)

    1..max_pos
    |> Enum.reduce(board, &add_position(&2, max_pos, &1))
  end

  def triangular_row(n) do
    triangular_stream()
    |> Enum.take(n)
    |> List.last()
  end

  defp add_position(board, max_pos, pos) do
    pegged = put_in(board, [Access.key(pos, %{}), Access.key(:pegged)], true)

    pegged
    |> connect_right(max_pos, pos)
    |> connect_down_left(max_pos, pos)
    |> connect_down_right(max_pos, pos)
  end

  defp connect(board, max_pos, pos, neighbor, destination) do
    if in_bounds?(max_pos, [neighbor, destination]) do
      Enum.reduce([{pos, destination}, {destination, pos}], board, fn {p1, p2}, nboard ->
        put_in(
          nboard,
          [Access.key(p1, %{}), Access.key(:connections, %{}), Access.key(p2)],
          neighbor
        )
      end)
    else
      board
    end
  end

  defp connect_right(board, max_pos, pos) do
    neighbor = pos + 1
    destination = 1 + neighbor

    unless triangular?(neighbor) or triangular?(pos) do
      connect(board, max_pos, pos, neighbor, destination)
    else
      board
    end
  end

  defp connect_down_left(board, max_pos, pos) do
    row = row_number(pos)
    neighbor = row + pos
    destination = 1 + row + neighbor

    connect(board, max_pos, pos, neighbor, destination)
  end

  defp connect_down_right(board, max_pos, pos) do
    row = row_number(pos)
    neighbor = 1 + row + pos
    destination = 2 + row + neighbor

    connect(board, max_pos, pos, neighbor, destination)
  end

  defp triangular_stream do
    Stream.unfold({1, 0}, fn {n, sum} -> {n + sum, {n + 1, n + sum}} end)
  end

  defp triangular?(n) do
    tri =
      triangular_stream()
      |> Enum.take_while(&(n >= &1))

    n in tri
  end

  defp row_number(n) do
    triangular_stream()
    |> Stream.take_while(&(n > &1))
    |> Enum.count()
    |> Kernel.+(1)
  end

  defp in_bounds?(max_pos, positions) do
    Enum.all?(positions, &(&1 <= max_pos))
  end
end
