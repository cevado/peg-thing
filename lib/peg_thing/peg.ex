defmodule PegThing.Peg do
  def pegged?(board, pos) do
    board[pos][:pegged]
  end

  def remove(board, pos) do
    put_in(board, [pos, :pegged], false)
  end

  def place(board, pos) do
    put_in(board, [pos, :pegged], true)
  end

  def move(board, from, to) do
    board
    |> remove(from)
    |> place(to)
  end

  def valid_moves(board, pos) do
    board[pos][:connections]
    |> Enum.filter(fn {destination, jumped} ->
      !pegged?(board, destination) and pegged?(board, jumped)
    end)
    |> Enum.into(%{})
  end

  def valid_move?(board, from, to) do
    valid_moves(board, from)[to]
  end

  def make_move(board, from, to) do
    case valid_move?(board, from, to) do
      nil ->
        board

      jumped ->
        board
        |> remove(jumped)
        |> move(from, to)
    end
  end

  def can_move?(board) do
    board
    |> Map.drop([:rows])
    |> Enum.filter(&elem(&1, 1)[:pegged])
    |> Enum.map(&elem(&1, 0))
    |> Enum.any?(&(valid_moves(board, &1) != %{}))
  end
end
